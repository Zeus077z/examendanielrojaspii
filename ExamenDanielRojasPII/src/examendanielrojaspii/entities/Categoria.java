/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examendanielrojaspii.entities;

/**
 *
 * @author Daniel
 */
public class Categoria {
    protected int id;
    protected String categoria;
    protected boolean activo;

    public Categoria() {
    }   

    public Categoria(int id, String categoria, boolean activo) {
        this.id = id;
        this.categoria = categoria;
        this.activo = activo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "Categoria{" + "id=" + id + ", categoria=" + categoria + ", activo=" + activo + '}';
    }
    
}
